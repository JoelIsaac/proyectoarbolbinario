#Joel Isaac Gutierrez Morales
class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None


class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        
        nuevo_nodo = Nodo(elemento) #genera un objeto nodo en nuevo nodo
        #Si la cabeza esta vacia
        if self.raiz is None:
            self.raiz = nuevo_nodo
            
        else:
            self.insertar_recursivo(self.raiz, elemento)
            

    def insertar_recursivo(self, lugar_nodo, elemento):
        nuevo_nodo = Nodo(elemento)
        
        if nuevo_nodo.valor > lugar_nodo.valor:
            if lugar_nodo.derecha is None:
                lugar_nodo.derecha = nuevo_nodo
            else:
                self.insertar_recursivo(lugar_nodo.derecha, nuevo_nodo.valor)
        else:
            if lugar_nodo.izquierda is None:
                lugar_nodo.izquierda = nuevo_nodo
            else:
                self.insertar_recursivo(lugar_nodo.izquierda, nuevo_nodo.valor)

                

    def eliminar(self, elemento):
        """
        if self.raiz.valor == elemento:
            self.raiz.valor = None
            self.raiz.derecha = None
            self.raiz.izquierda = None
            
        if self.existe(elemento):

            
        else:
            return
        """
        pass


    def existe(self, elemento):
        if self.busqueda_recursiva(self.raiz, elemento) is not None:
            return True
        else: return False


    def busqueda_recursiva(self,nodo,elemento_a_buscar):
        if nodo is None:
            return None
        if nodo.valor == elemento_a_buscar:
            return nodo
        if elemento_a_buscar > nodo.valor:
            return self.busqueda_recursiva(nodo.derecha, elemento_a_buscar)
        else: 
            return self.busqueda_recursiva(nodo.izquierda, elemento_a_buscar)


    def maximo(self):
        if self.raiz is None:
            return None
        
        if self.raiz.derecha is None:
            return self.raiz.valor
        
        return self.extremo_derecho(self.raiz).valor
    
    def extremo_derecho(self, nodo_base):
        if nodo_base.derecha == None:
            return nodo_base
        else:
            return self.extremo_derecho(nodo_base.derecha)
        
    def minimo(self):
        
        if self.raiz is None:
            return None
        if self.raiz.izquierda is None:
            return self.raiz.valor
        
        lugar = self.raiz

        while lugar.izquierda is not None:

            if lugar.izquierda.izquierda is None:
                break
            else: #lugar.derecha.derecha is not None
                nodo_izquierda = lugar.izquierda
                lugar = nodo_izquierda

        return lugar.izquierda.valor
        

    def altura(self):
        return self.altura_aux(self.raiz)
    
    def altura_aux(self, nodo):
        if nodo is None:
            return 0
        else:
            return max(self.altura_aux(nodo.izquierda), self.altura_aux(nodo.derecha)) +1

    
    def obtener_elementos(self):
        lista = [] 
        self.recorrido(self.raiz, lista)
        return lista
    
    def recorrido(self, nodo, lista):
        if nodo is not None:
            self.recorrido(nodo.izquierda, lista)
            lista.append(nodo.valor)
            self.recorrido(nodo.derecha, lista)

        

    def __str__(self):
        return "[ "+" ".join(map(str, sorted(self.obtener_elementos()))) + " ]"